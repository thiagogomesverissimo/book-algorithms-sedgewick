import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class Client {
	public static void main(String[] args) {

		StdOut.printf("#### Iniciando testes com filas ####");

		// Criando a fila
		Queue<String> queue = new Queue<String>();

		StdOut.printf("\n A fila está vazia? : %b", queue.isEmpty());
		StdOut.printf("\n Tamanho da fila: %d", queue.size());

		StdOut.printf("\n Adicionando na fila: Thiago, Thais, Arthur, Petrucio");
		queue.enqueue("Thiago");
		queue.enqueue("Thais");
		queue.enqueue("Arthur");
		queue.enqueue("Petrucio");

		StdOut.printf("\n Removendo %s. Pois está na fila há tempos...", queue.dequeue());
		StdOut.printf("\n Removendo %s. Pois está na fila há tempos...", queue.dequeue());
		StdOut.printf("\n Removendo %s. Pois está na fila há tempos...", queue.dequeue());
		StdOut.printf("\n A fila está vazia? : %b", queue.isEmpty());
		StdOut.printf("\n Tamanho da fila: %d", queue.size());

		// adicionado mais pessoas
		queue.enqueue("Sara");
		queue.enqueue("Jeremias");
		queue.enqueue("Pedro");
		queue.enqueue("Chico");
		StdOut.printf("\n A fila está vazia? : %b", queue.isEmpty());
		StdOut.printf("\n Tamanho da fila: %d", queue.size());
	}
}
