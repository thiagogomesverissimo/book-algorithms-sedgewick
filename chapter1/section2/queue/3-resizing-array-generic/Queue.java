/* Métodos da Fila:
 * void enqueue(Item item) - adiciona um item no final da fila
 * Item dequeue()          - remove o primeiro cara da fila, ie, que tá nela a mais tempo
 * isEmpty()               - verifica se a fila está vazia
 * size()                  - retorna tamanho da fila
 */
public class Queue<Item> {

	private int N, first, last;
	private Item[] queue;

	// método construtor
	public Queue(){
		queue = (Item[]) new Object[2];
		first = 0;
		last = 0;
		N = 0;
	}

	public boolean isEmpty(){
		return N==0;
	}

	public int size(){
		return N;
	}

	public void enqueue(Item item) {
		if(N == queue.length) resize(2*queue.length);
		queue[last] = item;
		N++;
		last++;

		if(last == queue.length) last = 0;
	}

	public Item dequeue(){
		// falta adicionar a situação de quando a lista está vazia
		// e lancar um exception
		Item item = queue[first];
		queue[first] = null; // para atuação do garbage collection do java
		first++;
		N--;
		if(first == queue.length) first = 0;

		if (N>0 && N == queue.length/4) resize(queue.length/2);
		return item;
	}

	private void resize(int max) {
		Item[] newqueue = (Item[]) new Object[max];
		for (int i = 0; i < N ;i++){
			// Essa condição para achar o índice só consegui entender desenhando:
			// ie. (i+first) % queue.length
			// a lista, pois como o last pode está no começo do array, tudo se complica
			newqueue[i] = queue[ (i+first) % queue.length ];
		}
		queue = newqueue;
		first = 0;
		last = N;
	}
}
