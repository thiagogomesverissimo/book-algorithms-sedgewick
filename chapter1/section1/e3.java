import edu.princeton.cs.algs4.StdOut;

public class Exercise_1_1_3 {

   public static void main(String[] args){
       if(args.length != 3){
           StdOut.println("You should type 3 numbers. Try again");
           return;
       }

       int a = Integer.parseInt(args[0]);
       int b = Integer.parseInt(args[1]);
       int c = Integer.parseInt(args[2]);
       check(a, b, c);
       return;
   }

   public static void check(int a, int b, int c){
       if(a == b && b == c){
           StdOut.println("Equal");
       } else {
           StdOut.println("Not Equal");
       }
   }  
}