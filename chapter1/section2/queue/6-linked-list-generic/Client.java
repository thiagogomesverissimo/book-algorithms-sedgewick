import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class Client {

	public static void main(String[] args) {
		Queue<String> queue = new Queue<String>();

		StdOut.printf("\n Tamanho da lista: %d", queue.size());
		StdOut.printf("\n A lista está vazia?: %b", queue.isEmpty());

		queue.enqueue("1. Estou na lista");
		queue.enqueue("2. Estou na lista");
		queue.enqueue("3. Estou na lista");
        StdOut.printf("\n Tamanho da lista: %d", queue.size());


		StdOut.printf("\n Removendo o primeiro da lista: %s",queue.dequeue());
		StdOut.printf("\n Tamanho da lista: %d", queue.size());

	}

}
