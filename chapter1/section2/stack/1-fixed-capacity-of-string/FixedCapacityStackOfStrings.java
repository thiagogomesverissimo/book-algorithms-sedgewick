public class FixedCapacityStackOfStrings {
    private int N;
    private String[] stack;

    public FixedCapacityStackOfStrings(int max) {
        stack = new String[max];
    }

    public void push(String item) {
        stack[N] = item;
        N = N + 1;
    }

    public String pop() {
        N = N - 1;
        return stack[N];
    }

    public boolean isEmpty() {
        return N==0;
    }

    public int size() {
        return N;
    }
}
