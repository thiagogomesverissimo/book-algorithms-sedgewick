Instalar algos4:

    cd ~
    wget https://algs4.cs.princeton.edu/code/algs4.jar

Em ~/.bashrc inserir:

    export CLASSPATH=$CLASSPATH:~/algs4.jar

Soluções bem bacanas:

https://github.com/reneargento/algorithms-sedgewick-wayne
