import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class Client {

    public static void main (String[] args) {
        FixedCapacityStackOfStrings stack = new FixedCapacityStackOfStrings(100);
        String item;
        while (!StdIn.isEmpty()) {
            item = StdIn.readString();
            if (!item.equals("-")) {
                stack.push(item);
            } else {
                StdOut.printf("\nFui removido: %s ", stack.pop());
            }
        }
        StdOut.printf("\nTamanho da pilha: %d", stack.size());
    }
    
}
