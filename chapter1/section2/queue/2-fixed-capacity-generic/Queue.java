/* Métodos da Fila:
 * void enqueue(Item item) - adiciona um item no final da fila
 * Item dequeue()          - remove o primeiro cara da fila, ie, que tá nela a mais tempo
 * isEmpty()               - verifica se a fila está vazia
 * size()                  - retorna tamanho da fila
 */

import java.util.NoSuchElementException;

public class Queue<Item> {

	private int N, first, last;
	private Item[] queue;

	// Método Contrutor
	public Queue(int max) {
		queue = (Item[]) new Object[max];
		N = 0;
		first = 0;
		last = 0;
	}

	public int size() {
		return N;
	}

	public boolean isEmpty(){
		return N==0;
	}

	public void enqueue(Item item) {
		queue[last] = item;
		N++;
		last++;
		if(last == queue.length) last=0;
	}

	public Item dequeue() {
		if (isEmpty()) throw new NoSuchElementException("Estou vazia");
		Item item = queue[first];
		queue[first] = null;
		first++;
		N--;
		if (first == queue.length) first = 0;
		return item;
	}

}
