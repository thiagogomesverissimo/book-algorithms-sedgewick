import java.util.Iterator;

public class IteratorStack<Item> implements Iterable<Item> {
    private int N;
    private Item[] stack;

    public Iterator<Item> iterator(){
        return new ReverseArrayIterator();
    }

    // nested class
    private class ReverseArrayIterator implements Iterator<Item>{
        private int i = N;
        public boolean hasNext(){
            return i > 0;
        }
        public Item next() {
            return stack[--i];
        }
        public void remove(){
            throw new UnsupportedOperationException();
        }
    }

    public IteratorStack() {
        stack = (Item[]) new Object[1];
        N = 0;
    }

    private void resize(int max) {
        Item[] temp= (Item[]) new Object[max];
        for (int i = 0; i < N ;i++) {
            temp[i] = stack[i]; 
        }
        stack = temp;
    }

    public void push(Item item) {
        if( stack.length  == N ) {
            resize(2*N);
        }
        stack[N] = item;
        N = N + 1;
    }

    public Item pop() {
        N = N - 1;
        Item item = stack[N]; 
        stack[N] = null; // para o garbage collection limpar a memória
        if( N > 0 && N == stack.length/4 ) {
            resize(stack.length/2);
        }
        return item;
    }

    public boolean isEmpty() {
        return N==0;
    }

    public int size() {
        return N;
    }
}
