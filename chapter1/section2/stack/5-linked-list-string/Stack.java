/* métodos stack: isEmpty(), size(), push(), pop() */

public class Stack {

    private int N;
    private Node first

    public int size() {
        return N;
    }

    public boolean isEmpty(){
        return N==0;
    }
    
    // Constructor
    public Stack(){

    }

    private class Node {
        String item;
        Node next;
    }
}
