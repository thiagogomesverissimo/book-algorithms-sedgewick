/* Métodos da Fila:
 * void enqueue(Item item) - adiciona um item no final da fila
 * Item dequeue()          - remove o primeiro cara da fila, ie, que tá nela a mais tempo
 * isEmpty()               - verifica se a fila está vazia
 * size()                  - retorna tamanho da fila
 */

import java.util.NoSuchElementException;

// Tem uma sacada na lista: confirma nós vamos usando o método dequeue
// vão ficando buracos no começo da lista, assim, quando usamos o enqueue e 
// estivermos no final do array, adicionamos no começo, assim, conseguimos usar
// o array de forma eficiente.

public class Queue {

	private int N, first, last;
	private String[] queue;

	// Método Construtor
	public Queue(int max) {
		queue = new String[max];
		N = 0;
		first = 0;
		last = 0;
	}

	public boolean isEmpty() {
		return N==0;
	}

	public int size() {
		return N;
	}

	public void enqueue(String item) {
		queue[last] = item;
		N = N + 1;
		last = last + 1;
		// Volta para o começo da lista caso o array acabe
		if(last == queue.length) last = 0;
	}

	public String dequeue(){
		if (isEmpty()) throw new NoSuchElementException("This Queue is empty!");

		String current_first = queue[first];
		queue[first] = null;
		first = first + 1;
		N = N - 1;

		// se o first chegar na última posição do array, ele deve voltar para o começo
		if(first == queue.length) first = 0;

		return current_first;
	}
}
