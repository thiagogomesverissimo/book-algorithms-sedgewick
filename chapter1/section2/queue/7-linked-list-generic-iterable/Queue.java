/* Métodos da Fila:
 * void enqueue(Item item) - adiciona um item no final da fila
 * Item dequeue()          - remove o primeiro cara da fila, ie, que tá nela a mais tempo
 * isEmpty()               - verifica se a fila está vazia
 * size()                  - retorna tamanho da fila
 */

import java.util.Iterator;

public class Queue<Item> implements Iterable<Item> {

	private Node first,last;
	private int N;

	// Método construtor
	public Queue(){
		first = new Node();
		last = new Node();
		N = 0;
	}

	public boolean isEmpty(){
		return N==0;
	}

	public int size(){
		return N;
	}

	public void enqueue(Item item) {
		Node oldlast = last;
		last = new Node();
		last.item = item;
		last.next = null;
		if( isEmpty()) first = last;
		else oldlast.next = last;
		N++;
	}

	public Item dequeue(){
		// teríamos que tratar o caso da lista vazia
		Item item = first.item;
		first = first.next;
		N--;
		if (isEmpty()) last = null;
		return item;
	}
	
	// classes auxiliares
	private class Node {
		Item item ;
		Node next;
	}

    // implementação do iterable
    public Iterator<Item> iterator() {
        return new QueueIterator();
    }

    // obrigatórios: hasNext, next, remove
    private class QueueIterator implements Iterator<Item> {

        private Node current = first;

        public boolean hasNext() {
            return current != null;
        }

        public Item next(){
            Item item = current.item;
            current = current.next;
            return item;
        }

        public void remove(){
        }
    }

}
