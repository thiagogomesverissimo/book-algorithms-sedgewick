import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class Client {

    public static void main (String[] args) {
		// métodos que stack deve ter: isEmpty, push, pop, size
        Stack<String> stack = new Stack<String>();

		// meus testes
		StdOut.printf("######### Meus Testes ###########");
        stack.push("1. Sou um valor da lista");
        stack.push("2. Sou um valor da lista");
        stack.push("3. Sou um valor da lista");
        stack.push("4. Sou um valor da lista");

		// verificando itens da lista
		StdOut.printf("\n Tamanho da lista %d", stack.size());
		StdOut.printf("\n Removendo o último: %s", stack.pop());
		for (String item : stack) {
			StdOut.printf("\n %s", item);
		}
	
		StdOut.printf("\n####### book tests: rodar assim: java Client < exemplo.txt #######");
		while (!StdIn.isEmpty()) {
			String item = StdIn.readString();
			if (!item.equals("-")) {
				stack.push(item);
			} else {
				StdOut.printf("\nFui removido: %s ", stack.pop());
            }
        }
    }
}
