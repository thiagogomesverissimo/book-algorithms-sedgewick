import edu.princeton.cs.algs4.StdOut;

public class Exercise_1_1_5
{
    public static void main(String[] args){

        if(args.length != 2){
            StdOut.println("Please man, provide me two numbers");
            return;
        }
        double x = Double.parseDouble(args[0]);
        double y = Double.parseDouble(args[1]);
        if(between(x) && between(y)) 
            StdOut.println("The two numbers provided are between 0 and 1");
        else 
            StdOut.println("The two numbers provided are NOT between 0 and 1");
    }

    public static boolean between(double i){
        if( (i > 0.0 && i < 1.0)) return true;
        return false;
    }

}