import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class Exercise_1_1_9
{
    public static void main(String[] args){
        int dividendo = Integer.parseInt(args[0]);
        StdOut.println("Java calculation: " + Integer.toBinaryString(dividendo));
        /* Para converter de decimal para binário vou fazer divisões inteira até que
         o dividendo seja maior que 0 */
        int divisor = 2;
        int resto;
        int quociente;
        /* Representação em uma string */
        String string = "";
        while(dividendo > 0){
            resto = dividendo % divisor;
            quociente = dividendo / divisor;
            dividendo = quociente;
            string = resto + string;
        }
        StdOut.println("  My calculation: " + string);
    }
}