import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdRandom;

public class e31
{
    public static void main(String[] args){
        int N = Integer.parseInt(args[0]);
        double p = Double.parseDouble(args[1]);
        double[][] d = new double[N][2];

        StdDraw.setCanvasSize(1024,1024);
        StdDraw.setScale(-1.0,1.0);
        
        // Espessura da bolinha do StdDraw.point
        StdDraw.setPenRadius(0.015);

        for(int i=0;i < N;i++){
            d[i][0] = Math.cos(2*Math.PI*i/N);
            d[i][1] = Math.sin(2*Math.PI*i/N);
            // point(double x, double y)
            StdDraw.point(d[i][0],d[i][1]);
        }

        // Voltando ao padrão, senão StdDraw.line irá assumir a espessura de 0.015
        StdDraw.setPenRadius();
        
        for(int i = 0; i < N-1;i++){
            for(int j = 1+i; j<N; j++){
                if(StdRandom.bernoulli(p))
                // line(double x1, double y1, double x2, double y2)
                StdDraw.line(d[i][0],d[i][1],d[j][0],d[j][1]);
            }
        }
    }
}