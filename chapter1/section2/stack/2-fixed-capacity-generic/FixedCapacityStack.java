public class FixedCapacityStack<Item> {
    private int N;
    private Item[] stack;

    public FixedCapacityStack(int max) {
        //stack = new Item[max]; Java não permite
        stack = (Item[]) new Object[max];
    }

    public void push(Item item) {
        stack[N] = item;
        N = N + 1;
    }

    public Item pop() {
        N = N - 1;
        return stack[N];
    }

    public boolean isEmpty() {
        return N==0;
    }

    public int size() {
        return N;
    }
}
