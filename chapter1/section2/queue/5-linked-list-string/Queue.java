/* Métodos da Fila:
 * void enqueue(Item item) - adiciona um item no final da fila
 * Item dequeue()          - remove o primeiro cara da fila, ie, que tá nela a mais tempo
 * isEmpty()               - verifica se a fila está vazia
 * size()                  - retorna tamanho da fila
 */
public class Queue {

    private int N;
    private Node first, last;

    // método construtor
    public Queue(){
        N = 0;
        first = new Node();
        last = new Node();
    }

    public int size(){
        return N;
    }

    public boolean isEmpty(){
        return N==0;
    }

    public void enqueue(String item) {
        Node oldlast = last;
        last = new Node();
        last.item = item;
        last.next = null;
        
        if(isEmpty()) first = last;
        else oldlast.next = last;
        N++;
    }

    public String dequeue() {
        if(isEmpty()) return "Estou vazia";

        Node oldfirst = first;
        first = first.next;
        N--;
        if (isEmpty()) last = null;

        return oldfirst.item;
    }
    
    class Node {
        String item;
        Node next;
    }

}
