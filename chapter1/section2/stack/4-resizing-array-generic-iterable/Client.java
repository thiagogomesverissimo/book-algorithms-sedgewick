import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class Client {

    public static void main (String[] args) {
        IteratorStack<String> stack = new IteratorStack<String>();
        String item;
        while (!StdIn.isEmpty()) {
            item = StdIn.readString();
            if (!item.equals("-")) {
                stack.push(item);
            } else {
                StdOut.printf("\nFui removido: %s ", stack.pop());
            }
        }
        StdOut.printf("\nTamanho da pilha: %d", stack.size());

        // testando se a pilha realmente é iterável
        for (String s : stack) {
            StdOut.printf("\n # Item da pilha: %s",s);
        }
    }   
}
