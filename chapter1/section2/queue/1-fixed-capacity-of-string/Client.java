import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class Client {
	public static void main(String[] args) {

		StdOut.printf("#### Iniciando testes com filas ####");

		// Criando a fila com capacidade para 100 itens
		Queue queue = new Queue(4);

		StdOut.printf("\n A fila está vazia? : %b", queue.isEmpty());
		StdOut.printf("\n Tamanho da fila: %d", queue.size());

		StdOut.printf("\n Adicionando na fila: Thiago, Thais");
		queue.enqueue("Thiago");
		queue.enqueue("Thais");

		StdOut.printf("\n Removendo %s. Pois está na fila há tempos...", queue.dequeue());
		StdOut.printf("\n Removendo %s. Pois está na fila há tempos...", queue.dequeue());
		StdOut.printf("\n A fila está vazia? : %b", queue.isEmpty());
		StdOut.printf("\n Tamanho da fila: %d", queue.size());

		StdOut.printf("\n Adicionando Mariana na fila");
		queue.enqueue("Mariana");
		StdOut.printf("\n Tamanho da fila: %d", queue.size());
	}
}
