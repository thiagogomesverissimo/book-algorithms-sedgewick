/* Métodos da Fila:
 * void enqueue(Item item) - adiciona um item no final da fila
 * Item dequeue()          - remove o primeiro cara da fila, ie, que tá nela a mais tempo
 * isEmpty()               - verifica se a fila está vazia
 * size()                  - retorna tamanho da fila
 */
public class Queue<Item> {

    private int N;
    private Node first,last;

    // Constructor
    public Queue(){
        N = 0;
        first = new Node();
        last = new Node();
    }

    public int size(){
        return N;
    }

    public boolean isEmpty(){
        return N==0;
    }

    public void enqueue(Item item){
        Node oldlast = last;
        last = new Node();
        last.item = item;
        last.next = null;
        if(isEmpty()) first = last;
        else oldlast.next = last;
        N++;
    }

    public Item dequeue(){
        // teria que fazer uma exceção aqui caso a lista esteja vazia
        Node oldfirst = first;
        first = first.next;
        if(isEmpty()) last = null;
        N--;
        return oldfirst.item;
    } 

    class Node {
        Item item;
        Node next;
    }
}
