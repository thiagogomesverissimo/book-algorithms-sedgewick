import java.util.Iterator;

public class Stack<Item> implements Iterable<Item> {

    private int N;
    private Node first;

	// construtor
	public Stack() {
        N = 0;
		first = new Node();
    }

	private class Node {
		Item item;
		Node next;
	}

	public Iterator<Item> iterator(){
        return new ListIterator();
    }

    // nested class
    private class ListIterator implements Iterator<Item>{
		private Node current = first;

        public boolean hasNext(){
            return current.next != null;
        }
        public Item next() {
           Item item = current.item;
		   current = current.next;
		   return item;
        }
        public void remove(){
            throw new UnsupportedOperationException();
        }
    }

    public void push(Item item) {
		// No caso da pilha, o cara que entra será sempre o primeiro
		Node oldfirst = first;
		first = new Node();
		first.item = item;
		first.next = oldfirst;
		N++;
    }

    public Item pop() {
        // remover o atual primeiro da pilha
		Item pop_item = first.item;
		first = first.next;
		N--;
		return pop_item;
    }

    public boolean isEmpty() {
        return N==0;
    }

    public int size() {
        return N;
    }
}
